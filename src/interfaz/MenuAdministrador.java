/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaz;

import entidades.*;
import static interfaz.Sistema.usuarioActivo;
import java.util.*;
import usuarios.Administrador;

/** Declaracion de la clase MenuAdministrador.
 *
 * @author Brank
 * @version 09/07/2019
 */
public class MenuAdministrador extends Menu {
    /** Creacion del constructor.
     * 
     * @param opciones Indica las distintas opciones del menu del Administrador.
     */
    public MenuAdministrador(ArrayList<String> opciones) {
        super(opciones);
    }

    @Override
    /** Metodo cargarMenu() Metodo sobrescrito de la clase padre Menu, que inicia el menu del Administrador.
     * 
     */
    public void cargarMenu() {
        boolean cerrarSesion = false;
        String nombreEmpresa;
        String direccionEstablecimiento;
        Empresa empresa;
        Establecimiento est;
        while (!(cerrarSesion)) {            
            mostrarMenu();
            System.out.print("Ingrese opción : ");
            String opcion = Util.ingresoString();
            while ((!(Util.isNumeric(opcion))) || (!(Util.isBetween(1, 5, opcion)))) {
                System.out.print("Opción incorrecta. Ingrese nuevamente : ");
                opcion = Util.ingresoString();
            }
            switch (opcion) {
                case "1":
                    Administrador.miCuenta((Administrador) usuarioActivo);
                    break;
                case "2":
                    Empresa.registrarEmpresa();
                    break;
                case "3":
                    System.out.print("Ingrese la empresa propietaria del establecimiento : ");
                    nombreEmpresa = Util.ingresoString();
                    empresa = Empresa.comprobarEmpresa(nombreEmpresa);
                    if (!(empresa == null)) {
                        Establecimiento.registrarEstablecimiento(empresa);
                    } else {
                        System.out.println();
                        System.out.println("La empresa ingresada no existe");
                        Util.continuar();
                    }
                    break;
                case "4":
                    System.out.print("Ingrese la empresa propietaria del establecimiento : ");
                    nombreEmpresa = Util.ingresoString();
                    empresa = Empresa.comprobarEmpresa(nombreEmpresa);
                    if (!(empresa == null)) {
                        System.out.print("Ingrese la dirección del establecimiento : ");
                        direccionEstablecimiento = Util.ingresoString();
                        est = Establecimiento.comprobarEstablecimiento(empresa, direccionEstablecimiento);
                        if (!(est == null)) {
                            Promocion.registrarPromocion(empresa, est);
                        } else {
                            System.out.println();
                            System.out.println("El establecimiento ingresado no existe");
                            Util.continuar();
                        }
                    } else {
                        System.out.println();
                        System.out.println("La empresa ingresada no existe");
                        Util.continuar();
                    }
                    break;
                case "5":
                    cerrarSesion = true;
                    break;                
            }
        }
    }
    /** Metodo añadirOpciones() añade las distintas opciones del menu.
     * 
     * @return 
     */
    public static ArrayList<String> añadirOpciones() {
        ArrayList<String> opciones = new ArrayList<>();
        opciones.add("Mi cuenta");
        opciones.add("Registrar empresa");
        opciones.add("Registrar establecimiento");
        opciones.add("Registrar promoción");
        opciones.add("Cerrar sesión");
        return opciones;
    }
}
